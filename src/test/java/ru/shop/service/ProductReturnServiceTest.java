package ru.shop.service;

import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.shop.exception.BadProductReturnCountException;
import ru.shop.model.*;
import ru.shop.repository.ProductReturnRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductReturnServiceTest {
    private final ProductReturnRepository productReturnRepository = Mockito.mock();
    private final ProductReturnService productReturnService = new ProductReturnService(productReturnRepository);

    @Test
    void shouldAddProductReturn() {
        // given
        Customer customer = new Customer(UUID.randomUUID(), "John", "+71234567890", 35);
        UUID uuid = UUID.randomUUID();
        Order order = new Order(customer, new Product(uuid, "Phone", 1000, ProductType.GOOD), 2);

        // when
        productReturnService.add(order, 1);

        // then
        verify(productReturnRepository).save(any());
    }

    @Test
    void shouldThrowBadProductReturnCountException() {
        // given
        Customer customer = new Customer(UUID.randomUUID(), "John", "+71234567890", 35);
        UUID uuid = UUID.randomUUID();
        Order order = new Order(customer, new Product(uuid, "Phone", 1000, ProductType.GOOD), 2);

        // when & then
        assertThrows(
                BadProductReturnCountException.class,
                () -> productReturnService.add(order, 3)
        );
    }

    @Test
    void shouldReturnAllProductReturns() {
        // given
        List<ProductReturn> productReturns = List.of(
                new ProductReturn(UUID.randomUUID(), LocalDate.now(), 2),
                new ProductReturn(UUID.randomUUID(), LocalDate.now(), 1)
        );
        when(productReturnRepository.findAll()).thenReturn(productReturns);

        // when
        List<ProductReturn> result = productReturnService.findAll();

        // then
        assertIterableEquals(productReturns, result);
    }

    @Test
    void shouldReturnById() {
        // given
        Customer customer = new Customer(UUID.randomUUID(), "John", "+71234567890", 35);
        Order order = new Order(customer, new Product(UUID.randomUUID(), "Phone", 1000, ProductType.GOOD), 2);
        ProductReturn productReturn = new ProductReturn(order.getId(), LocalDate.now(), 2);

        when(productReturnRepository.findById(productReturn.getId())).thenReturn(Optional.of(productReturn));

        // when
        ProductReturn productReturn1 = productReturnService.findById(productReturn.getId());

        // then
        assertEquals(productReturn, productReturn1);
    }

    @Test
    void shouldThrowEntityNotFoundException() {
        // given
        UUID uuid = UUID.randomUUID();
        when(productReturnRepository.findById(uuid)).thenThrow(EntityNotFoundException.class);

        // when & then
        assertThrows(
                EntityNotFoundException.class,
                () -> productReturnService.findById(uuid)
        );
    }
}

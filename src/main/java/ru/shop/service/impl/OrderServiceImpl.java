package ru.shop.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.shop.exception.BadOrderCountException;
import ru.shop.model.Customer;
import ru.shop.model.Order;
import ru.shop.model.Product;
import ru.shop.model.ProductType;
import ru.shop.repository.OrderRepository;
import ru.shop.repository.ProductRepository;
import ru.shop.service.OrderService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
    }


    public void add(Customer customer, Product product, long count) {
        if (count <= 0) {
            throw new BadOrderCountException("Count should not be less than 1");
        }

        Order order = new Order(customer, product, count);

        orderRepository.save(order);
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public List<Order> findAllByCustomer(Customer customer) {
        return orderRepository.findAll()
                .stream()
                .filter(o -> o.getCustomer().getId().equals(customer.getId()))
                .collect(Collectors.toList());
    }

    public long getTotalCustomerAmount(Customer customer) {
        return orderRepository.findAll()
                .stream()
                .filter(o -> o.getCustomer().getId().equals(customer.getId()))
                .mapToLong(Order::getAmount)
                .sum();
    }

    public long getCount() {
        return orderRepository.count();
    }

    public List<Order> findAllByProductType(ProductType productType) {
        return orderRepository.findAll()
                .stream()
                .filter(order -> order.getProduct().getProductType().equals(productType))
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> getByCustomerId(UUID id) {
        return orderRepository.findAll().stream()
                .filter(order -> order.getCustomer().getId().equals(id))
                .collect(Collectors.toList());
    }

    @Override
    public Order findById(UUID id) {
        return orderRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND)
        );
    }

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }
}

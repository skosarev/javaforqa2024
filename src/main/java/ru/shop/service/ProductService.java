package ru.shop.service;

import ru.shop.model.Product;
import ru.shop.model.ProductType;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    void save(Product product);

    List<Product> findAll();

    List<Product> findAllByProductType(ProductType productType);

    Product findById(UUID id);
}

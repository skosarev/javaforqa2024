package ru.shop.service;

import ru.shop.model.Customer;
import ru.shop.model.Order;
import ru.shop.model.Product;
import ru.shop.model.ProductType;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    void add(Customer customer, Product product, long count);

    List<Order> findAll();

    List<Order> findAllByCustomer(Customer customer);

    long getTotalCustomerAmount(Customer customer);

    long getCount();

    List<Order> findAllByProductType(ProductType productType);

    List<Order> getByCustomerId(UUID id);

    Order findById(UUID id);

    void save(Order order);
}

package ru.shop.service;

import ru.shop.model.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {
    void save(Customer customer);

    List<Customer> findAll();

    Customer findById(UUID id);
}

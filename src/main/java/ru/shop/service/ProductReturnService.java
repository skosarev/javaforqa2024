package ru.shop.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shop.exception.BadProductReturnCountException;
import ru.shop.model.Order;
import ru.shop.model.ProductReturn;
import ru.shop.repository.ProductReturnRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(readOnly = true)
public class ProductReturnService {
    private final ProductReturnRepository productReturnRepository;

    public ProductReturnService(ProductReturnRepository productReturnRepository) {
        this.productReturnRepository = productReturnRepository;
    }

    public void add(Order order, long count) {
        if (order.getCount() < count) {
            throw new BadProductReturnCountException();
        }

        productReturnRepository.save(new ProductReturn(UUID.randomUUID(), LocalDate.now(), count));
    }

    public List<ProductReturn> findAll() {
        return productReturnRepository.findAll();
    }

    public ProductReturn findById(UUID id) {
        return productReturnRepository.findById(id).orElseThrow(
                EntityNotFoundException::new
        );
    }
}

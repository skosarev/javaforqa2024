package ru.shop.exception;

public class BadOrderCountException extends RuntimeException {

    public BadOrderCountException() {
    }

    public BadOrderCountException(String message) {
        super(message);
    }
}

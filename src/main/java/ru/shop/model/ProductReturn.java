package ru.shop.model;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter

@Setter

@EqualsAndHashCode
@Table(name = "product_return")
public class ProductReturn {
    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "order_id")
    private UUID orderId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "quantity")
    private long quantity;

    public ProductReturn(UUID orderId, LocalDate date, long quantity) {
        this.orderId = orderId;
        this.date = date;
        this.quantity = quantity;
    }
}

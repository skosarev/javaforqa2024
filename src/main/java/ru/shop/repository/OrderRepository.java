package ru.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shop.model.Order;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
}

package ru.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shop.model.Product;

import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
}

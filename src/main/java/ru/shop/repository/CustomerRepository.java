package ru.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shop.model.Customer;

import java.util.UUID;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {
}
